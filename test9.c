//write your code here
#include<stdio.h>
struct Name
{
	char firstname[25];
	char lastname[25];
};

struct student
{
	int r_no;
	struct Name n;
	char department[3];
	float fees;
	char section[2];
	int Totalmarks;
};

int main()
{
	struct student s[1];
	int i;

	for(i=0;i<1;i++)
	{
		printf("Enter the details of the student%d:\n\n",i+1);
		printf("Enter the roll number\n");
		scanf("%d",&s[i].r_no);
		printf("Enter the firstname\n");
		scanf("%s", s[i].n.firstname);
		printf("Enter the lastname\n");
		scanf("%s", s[i].n.lastname);
		printf("Enter the student's department\n");
		scanf("%s",s[i].department);
		printf("Enter the fees\n");
		scanf("%f",&s[i].fees);
		printf("Enter the section\n");
		scanf("%s",s[i].section);
		printf("Enter the total marks obtained out of 500\n");
		scanf("%d",&s[i].Totalmarks);
	}

	printf("\nStudent Details\n\n");
	for(i=0;i<1;i++)
	{
		printf("Roll number= %d\n",s[i].r_no);
		printf("First Name= %s\n",s[i].n.firstname);
		printf("Last Name= %s\n",s[i].n.lastname);
		printf("Department= %s\n",s[i].department);
		printf("Section= %s\n",s[i].section);
		printf("Fees= %0.2f\n",s[i].fees);
		printf("Totalmarks Obtained= %d\n",s[i].Totalmarks);
	}
	return 0;
}
