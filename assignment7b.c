#include<stdio.h>
int main()
{
    int n,i;
    float sum=0.00;
    float temp;
    printf("Enter the value of n-");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        temp=1/(pow(i,2));
        sum=sum+temp;
    }
    printf("\nSum of series 1/1^2 + 1/2^2 +...+1/%d^2- %f",n,sum);
    return 0;
}

OUTPUT:

Enter the value of n- 7
Sum of series 1/1^2 + 1/2^2 +...+1/7^2- 1.511797