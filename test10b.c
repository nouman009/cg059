#include<stdio.h>
void add(int *a, int *b, int *s);
void div(int *a, int *b, int *d);
void rem(int *a, int *b, int *r);
void mul(int *a, int *b, int *m);
int main()
{
	int num1,num2,sum,di,r,product;
	printf("Enter 2 numbers\n");
	scanf("%d %d",&num1,&num2);
	add(&num1,&num2,&sum);
	printf("Sum of 2 numbers is= %d\n",sum);
	div(&num1,&num2,&di);
	printf("Division of 2 numbers is= %d\n",di);
	rem(&num1,&num2,&r);
	printf("Remainder of 2 numbers is= %d\n",r);
	mul(&num1,&num2,&product);
	printf("Product of 2 numbers is= %d\n",product);
	return 0;
}

void add(int *a, int *b, int *s)
{
	*s= *a+*b;
}

void div(int *a, int *b, int *d)
{
	*d= (*a)/(*b);
}

void rem(int *a, int *b, int *r)
{
	*r= (*a)%(*b);
}

void mul(int *a, int *b, int *m)
{
	*m= (*a)*(*b);
}