//write your code here
#include<stdio.h>
#include<math.h>
int main()
{
	int a,b,c;
	float d;
	float root1,root2,imaginary;
	printf("Enter the values of a,b and c\n");
	scanf("%d %d %d",&a,&b,&c);
	d=(b*b)-(4*a*c);
	if(d>0)
	{
		root1=(-b+sqrt(d)/(2*a));
		root2=(-b-sqrt(d)/(2*a));
		printf("two distinct and real roots exists: %f and %f\n",root1,root2);
	}
	else if(d==0)
	{
		root1=root2=(-b)/(2*a);
		printf("two equal and real roots exists: %f and %f\n",root1,root2);
	}
	else if(d<0)
	{
		root1=root2=(-b)/(2*a);
		imaginary= sqrt(-d)/(2*a);
	}
	printf("Two distinct complex roots exist: %0.2f and %0.2f\n",root1,imaginary);
	return 0;
}
